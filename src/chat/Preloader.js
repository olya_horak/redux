import React from 'react';

class Preloader extends React.Component {
    render() {

        if (!this.props.isLoading) {
            return <div></div>
        }
        const loaderMessage = this.props.busyMessage || "Loading...";

        return (
            <div className="preloader">
                <i className="fa fa-spinner fa-spin"></i>
               {loaderMessage}
            </div>
        );
    }
}
export default Preloader;