import React from 'react';

class MessageInput extends React.Component {
    /*Повинен використовуватись для відправки та редагування повідомлення.*/

    constructor(props) {
        super(props);

        this.state = {
            myMessage: ""
        };
    }

    sendMessage = (args) => {
        this.props.onSentMessage(this.state.myMessage);
        this.setState({
            myMessage: ""
        });
    }

    updateMessage = (e) => {
        const value = e.target.value
        this.setState({
            myMessage: value
        });
    }


    render() {

        return (
            <div className="message-input">
                <input className="message-input-text" onChange={this.updateMessage} value={this.state.myMessage}>

                </input>

                <button className="message-input-button" onClick={this.sendMessage}>
                 Send
                </button>

            </div>
        );
    }
}
export default MessageInput;