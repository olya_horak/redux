import { ADD_MESSAGE, UPDATE_MESSAGE, DELETE_MESSAGE } from "./actionTypes";


const messages = [];

fetch("https://edikdolynskyi.github.io/react_sources/messages.json")
    .then((response) => {
        return response.json();

    })
    .then((messages) => {
        messages = messages.sort((m1, m2) => new Date(m1.createdAt) - new Date(m2.createdAt));
    });

const initialState = messages;

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_MESSAGE: {
            const { id, data } = action.payload;
            const newMessage = { id, ...data };
            return [...state, newMessage];
        }

        case UPDATE_MESSAGE: {
            const { id, data } = action.payload;
            const updatedMessages = state.map(message => {
                if (message.id === id) {
                    return {
                        ...message,
                        ...data
                    };
                } else {
                    return message;
                }
            });
            return updatedMessages;
        }

        case DELETE_MESSAGE: {
            const { id } = action.payload;
            const filteredMessages = state.filter(message => message.id !== id);
            return filteredMessages;
        }
        default:
            return state;
    }
}
