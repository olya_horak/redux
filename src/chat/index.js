import { Component } from "react";
import MessageList from "./MessageList";
import Header from "./Header";
import Preloader from "./Preloader";
import MessageInput from "./MessageInput";
import { connect } from "react-redux";
import * as actions from "./actions";
import {setCurrentMessageId, showPage} from "../message/actions";

class Chat extends Component {
    constructor(props) {
        super(props);
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onAdd = this.onAdd.bind(this);

        this.state = {
            messages: [],
            isLoading: false,
            myName: "Olya"
        };
    }

    onEdit(id) {
        this.props.setCurrentMessageId(id);
        this.props.showPage();
    }

    onDelete(id) {
        this.props.deleteMessage(id);
    }

    onAdd() {
        this.props.showPage();
    }


    render() {
        return (
            <div className="Chat flex-col">
                <Header messages={this.state.messages} />
                {this.props.chat.map(message => {
                    return (
                        <MessageList
                            onEdit={this.onEdit}
                            onDelete={this.onDelete} />
                    );
                })}
                <MessageInput
                    onSend={this.onAdd} />
                <Preloader isLoading={this.state.isLoading} />
            </div>
        );

    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages
    }
};

const mapDispatchToProps = {
    ...actions,
    setCurrentMessageId,
    showPage
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
