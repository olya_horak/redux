import React from 'react';
import moment from 'moment';
class Header extends React.Component {
    render() {
        const messages = this.props.messages || [];

        const uniqueUsers = new Set(messages.map(item => item.userId)).size;
        const messageCount = messages.length;

        /* форматі dd.mm.yyyy hh:mm (Наприклад: 15.02.2021 13:35) */
        const lastMessageDate = messages.map(function (item) { return item.createdAt; }).sort().reverse()[0]

        return (
            <div className="header">
                <div className="header-title">
                    BS Chat
                </div>
                <div >
                    <span className="header-users-count">{uniqueUsers}</span>participants
                </div>
                <div >
                    <span className="header-messages-count">{messageCount}</span>   messages
                </div>

                <div >
                    Last message at: <span className="header-last-message-date">
                        {moment(lastMessageDate).format("DD.MM.YYYY HH:mm")}
                        </span>
                </div>
            </div>
        );
    }
}
export default Header;
