
import React from 'react';
import moment from 'moment';

class OwnMessage extends React.Component {

    updateMessage = () => {
        
    }

    render() {
        const message = this.props.message;

        return (
            <div className="own-message flex-col">
                <div className="message-text">
                 {message.text}
                </div>

                {/*у форматі hh:mm */}
                <div className="message-time">
                 {moment(message.createdAt).format("HH:mm")}
                </div>

                <img className="message-user-avatar" src={message.avatar} alt="ava" />

                <button className="message-edit" onClick={this.updateMessage}>
                Edit
                </button>
               

                <button className="message-delete" onClick={this.props.onDeleteMessage}>
                Delete
                </button>

            </div>
        );
    }
}
export default OwnMessage;