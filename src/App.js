import React from 'react';
import Chat from './chat/index';
import Message from './message/index';

import './App.css';



function App() {
  return (
    <div className="App">
      <Chat />
      <Message />

    </div>
  );
}

export default App;
