import { Component } from "react";
import defaultMessageConfig from "../shared/defaultMessageConfig.json"
import MessageInput from '../chat'
import messageConfig from "../shared/messageConfig.json"

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultMessageData();
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.messgeId !== this.props.messgeId) {
            const message = this.props.messages.find(message => message.id === nextProps.messgeId);
            this.setState(message);
        }
    }

    onCancel() {
        this.props.dropCurrentMessageId();
        this.props.hidePage();
        this.setState(this.getDefaultMessageData());
    }

    onSave() {
        if (this.props.messageId) {
            this.props.updateMessage(this.props.messgeId, this.state);
            this.props.dropCurrentMessageId();
        } else {
            this.props.addMessage(this.state);
        }
        this.props.hidePage();
        this.setState(this.getDefaultMessageData());
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }

        );
    }

    getDefaultMessageData() {
        return {
            ...defaultMessageConfig
        };
    }

    getInput(data, keyword) {
        if (data) {
            return (
                <MessageInput
                    text={data} />
            )
        }
    }

    getMessagePage() {
        const data = this.state;

        return (
            <div className="modal" style={{ display: "block" }} tabIndex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{ padding: "5px" }}>
                        <div className="modal-header">
                            <button className="close" data-dismiss="modal" aria-label="Close" onClick={this.onCancel}>
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {
                                messageConfig.map(data.getInput(data, data))
                            }
                        </div>
                        <div className="modal-footer">
                            <button className="cancel" onClick={this.onCancel}>Cancel</button>
                            <button className="save" onClick={this.onSave}>SAve</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
    render() {
        const isShown = this.props.isShown;
        return isShown ? this.getMessagePage() : null;
    }
}

export default Message;