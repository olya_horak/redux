import Chat from './src/chat/index';
import rootReducer from './src/chat/reducer';

export default {
    Chat,
    rootReducer,
};